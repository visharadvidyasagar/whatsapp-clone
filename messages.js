const newmsg=[
    //chat1
    [
    {
      id:1,
      message:"Listen",
      type:"chat-sent",
      time: "08:06"
    },
    {
      id:2,
      message:"Did you finish those topics?",
      type:"chat-sent",
      time: "08:07"
    },
    {
      id:3,
      message:"Yes, I did",
      type:"chat-rcvd",
      time: "08:13"
    },
    {
      id:4,
      message:"Did 2 hours back",
      type:"chat-rcvd",
      time: "08:13"
    }
  ],
  //chat2
  [
    {
      id:1,
      message:"Hii",
      type:"chat-sent",
      time: "15:32"
    },
    {
      id:2,
      message:"Hi",
      type:"chat-rcvd",
      time: "15:36"
    },
    {
      id:3,
      message:"Are you attending session?",
      type:"chat-sent",
      time: "15:37"
    },
    {
      id:4,
      message:"Yes",
      type:"chat-rcvd",
      time: "15:38"
    },
    {
      id:5,
      message:"Are you attending?",
      type:"chat-rcvd",
      time: "15:38"
    },
    {
      id:6,
      message:"Yeah, I will",
      type:"chat-sent",
      time: "15:40"
    },
    {
      id:7,
      message:"The venue is bit far",
      type:"chat-sent",
      time: "15:40"
    },
    {
      id:8,
      message:"But will be helpful",
      type:"chat-sent",
      time: "15:41"
    },
    {
      id:9,
      message:"Cool, see you!",
      type:"chat-rcvd",
      time: "16:24"
    },
    {
      id:10,
      message:"Sure",
      type:"chat-sent",
      time: "16:27"
    }
  ],
  //chat3
  [
    {
      id:1,
      message:"Hello",
      type:"chat-rcvd",
      time: "17:12"
    },
    {
      id:2,
      message:"Will you go for the session tomorrow?",
      type:"chat-rcvd",
      time: "17:13"
    },
    {
      id:3,
      message:"Hi",
      type:"chat-sent",
      time: "17:35"
    },
    {
      id:4,
      message:"Yup, going there",
      type:"chat-sent",
      time: "17:36"
    },
    {
      id:5,
      message:"It will be pretty helpful",
      type:"chat-sent",
      time: "17:36"
    },
    {
      id:6,
      message:"Even though it's quite far",
      type:"chat-sent",
      time: "18:06"
    },
    {
      id:7,
      message:"Even you are joining us right?",
      type:"chat-sent",
      time: "18:07"
    },
    {
      id:8,
      message:"Obviously",
      type:"chat-rcvd",
      time: "19:03"
    },
    {
      id:9,
      message:"Why to miss this chance",
      type:"chat-rcvd",
      time: "19:04"
    },
    {
      id:10,
      message:"Okay Cool....",
      type:"chat-sent",
      time: "19:06"
    }
  ],
  //chat4
  [
    {
      id:1,
      message:"Mom, how are you?",
      type:"chat-sent",
      time: "20:15"
    },
    {
      id:2,
      message:"Doing good! What about you?",
      type:"chat-rcvd",
      time: "20:21"
    },
    {
      id:3,
      message:"Good! I was planning to enroll for a course",
      type:"chat-sent",
      time: "20:23"
    },
    {
      id:4,
      message:"It will be free for 2weeks",
      type:"chat-sent",
      time: "20:23"
    },
    {
      id:5,
      message:"Regarding what?",
      type:"chat-rcvd",
      time: "20:25"
    },
    {
      id:6,
      message:"DSA, DBMS, Web-Development",
      type:"chat-sent",
      time: "20:27"
    },
    {
      id:7,
      message:"I'll enroll??",
      type:"chat-sent",
      time: "20:28"
    },
    {
      id:8,
      message:"Yes sure!",
      type:"chat-rcvd",
      time: "20:31"
    },
    {
      id:9,
      message:"Thanks....",
      type:"chat-sent",
      time: "20:32"
    }
  ],
  //chat5
  [
    {
      id:1,
      message:"Send me the fee details",
      type:"chat-rcvd",
      time: "10:57"
    },
    {
      id:2,
      message:"Okay wait, mailed it to u",
      type:"chat-sent",
      time: "11:05"
    },
    {
      id:3,
      message:"okay, recieved",
      type:"chat-rcvd",
      time: "11:08"
    },
    {
      id:4,
      message:"When will they give you the reciept?",
      type:"chat-rcvd",
      time: "11:09"
    },
    {
      id:5,
      message:"Maybe by today evening",
      type:"chat-sent",
      time: "11:14"
    },
    {
      id:6,
      message:"I have asked for it today",
      type:"chat-sent",
      time: "11:14"
    },
    {
      id:7,
      message:"Okay",
      type:"chat-rcvd",
      time: "12:12"
    },
    {
      id:8,
      message:"Better scan it and send",
      type:"chat-rcvd",
      time: "12:14"
    },
    {
      id:9,
      message:"Sure",
      type:"chat-sent",
      time: "12:17"
    }
  ],
  //chat6
  [
    {
      id:1,
      message:"Hello Sir \n Good Evening",
      type:"chat-sent",
      time: "19:34"
    },
    {
      id:2,
      message:"Hi, how are you?",
      type:"chat-rcvd",
      time: "19:48"
    },
    {
      id:3,
      message: "Good Sir, Project completed...",
      type:"chat-sent",
      time: "19:52"
    },
    {
      id:4,
      message:"That's great...",
      type:"chat-rcvd",
      time: "22:07"
    }
  ],
  ]